class CreateIssues < ActiveRecord::Migration[5.1]
  def change
    create_table :issues do |t|
      t.text :content
      t.integer :user_id
      t.text :title
      t.integer :type
      t.integer :priority
      t.integer :status
      t.integer :votes
      t.string :assignee
      t.string :created
      t.string :updated

      t.timestamps
    end
  end
end
