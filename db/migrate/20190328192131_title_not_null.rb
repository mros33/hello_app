class TitleNotNull < ActiveRecord::Migration[5.1]
  def change
    change_column_null :issues, :title, false, "eraNull"
  end
end
