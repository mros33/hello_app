class Issues < ActiveRecord::Migration[5.1]
  def change
      change_column :issues, :tipus, :string
      change_column :issues, :priority, :string
      change_column :issues, :status, :string
  end
end
