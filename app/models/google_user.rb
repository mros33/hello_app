class GoogleUser < ActiveRecord::Base
    def self.from_omniauth(auth)
        where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |google_users|
          google_users.provider = auth.provider
          google_users.uid = auth.uid
          google_users.name = auth.info.name
          google_users.oauth_token = auth.credentials.token
          google_users.oauth_expires_at = Time.at(auth.credentials.expires_at)
          google_users.save!
    end
  end
end
