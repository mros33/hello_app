json.extract! issue, :id, :content, :user_id, :title, :tipus, :priority, :status, :votes, :assignee, :created, :updated, :created_at, :updated_at
json.url issue_url(issue, format: :json)
